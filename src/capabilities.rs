//! Various capabilities.

use crate::Error;
use dbus_async::DBus;
use dbus_message_parser::{Message, Value};
use std::sync::Arc;

pub mod available;
pub use available::AvailableCapabilities;

pub mod breath_effect;
mod brightness;
pub mod custom_effect;
mod game_mode;
mod keyboard_layout;
pub mod macro_effect;
mod macro_mode;
pub mod matrix_dimensions;
mod no_effect;
pub mod reactive_effect;
pub mod ripple_effect;
mod spectrum_effect;
pub mod starlight_effect;
mod static_effect;
pub mod wave_effect;

pub use breath_effect::BreathEffect;
pub use brightness::Brightness;
pub use custom_effect::CustomEffect;
pub use game_mode::GameMode;
pub use keyboard_layout::KeyboardLayout;
pub use macro_effect::MacroEffect;
pub use macro_mode::MacroMode;
pub use matrix_dimensions::MatrixDimensions;
pub use no_effect::NoEffect;
pub use reactive_effect::ReactiveEffect;
pub use ripple_effect::RippleEffect;
pub use spectrum_effect::SpectrumEffect;
pub use starlight_effect::StarlightEffect;
pub use static_effect::StaticEffect;
pub use wave_effect::WaveEffect;

#[derive(Debug, Clone)]
pub(crate) struct CapabilityBase {
    dbus: DBus,
    path: Arc<str>,
}

impl CapabilityBase {
    pub(crate) fn new(dbus: DBus, path: Arc<str>) -> Self {
        Self { dbus, path }
    }

    fn message(&self, interface: &'static str, method: &'static str) -> Message {
        Message::method_call(crate::DESTINATION, &self.path, interface, method)
    }

    async fn call_get_method(
        &self,
        interface: &'static str,
        method: &'static str,
    ) -> Result<Value, Error> {
        let (_, body) = self
            .dbus
            .call(self.message(interface, method))
            .await?
            .split();

        if body.len() == 1 {
            Ok(body.into_iter().next().unwrap())
        } else {
            Err(Error::InvalidResponse)
        }
    }

    async fn call_set_method(
        &self,
        interface: &'static str,
        method: &'static str,
        body: Vec<Value>,
    ) -> Result<(), Error> {
        let mut message = self.message(interface, method);
        body.into_iter().for_each(|value| message.add_value(value));
        let (_, body) = self.dbus.call(message).await?.split();

        if body.is_empty() {
            Ok(())
        } else {
            Err(Error::InvalidResponse)
        }
    }
}
